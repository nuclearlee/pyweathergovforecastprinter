# this is in python 3
import requests
import sys
import collections


# struct containing weather stuffs i'm interested in
class Weather:
    def __init__(self, tempF=0, windMPH=0, windGustMPH=0, windDir="", windDirDeg=0):
        self.tempF = tempF
        self.windMPH = windMPH
        self.windGustMPH = windGustMPH
        self.windDir = windDir
        self.windDirDeg = windDirDeg


class CurrentWeather():
    def __init__(self, location="", stationId="", epoch=0,
                 tempF=0, windMPH=0, windGustMPH=0, windDir="",
                 windDirDeg=0, precipTodayIn=0, timeStampUTC=""):
        self.location = location
        self.stationId = stationId
        self.epoch = epoch
        self.tempF = tempF
        self.windMPH = windMPH
        self.windGustMPH = windGustMPH
        self.windDir = windDir
        self.windDirDeg = windDirDeg
        self.precipTodayIn = precipTodayIn
        self.timeStampUTC = timeStampUTC


class FutureWeather:
    def __init__(self, tempHigh=0, tempLow=0, windAve=0, windDescr="", dayName="", simpleTextForecastDay="",
                 simpleTextForecastNight=""):
        self.tempHigh = tempHigh
        self.tempLow = tempLow
        self.windAve = windAve
        self.windDescr = windDescr
        self.dayName = dayName
        self.simpleTextForecastDay = simpleTextForecastDay
        self.simpleTextForecastNight = simpleTextForecastNight

class RequestNotOkError(Exception):
    pass
        
CityStateForecastSites = collections.namedtuple('CityStateForecastSites', ['city', 'state', 'forecastsimple', 'forecasthourly', 'closestconditionssite', 'closestconditionsiteid'])

CityStateForecastSites = collections.namedtuple('CityStateForecastSites',
                                                ['city', 'state', 'forecastsimple', 'forecasthourly',
                                                 'closestconditionssite', 'closestconditionsiteid'])

weatherGovEntrySite = "https://api.weather.gov/points"

# latitude/longitude, which will give you the forecast at that location,
# edgewood
latlong = "35.127369,-106.209082"
# tingley beach
# latlong = "35.088933,-106.677203"
# palo duro canyon
# latlong= "34.937352,-101.6490683"
# denver, because new mexico is broken
# latlong ="39.739427,-104.925868"

# uncomment one of the above lines or add your own.
# this is probably an option you could use
LOCATION = latlong


def CelsiusToFahrenheit(degC):
    degF = degC * 9.0 / 5.0 + 32
    return degF


def FahrenheitToCelsius(degF):
    degC = (degF - 32) * 5.0 / 9.0
    return degC

def MetersPerSecondToMilesPerHour(speedMetersPerSecond):
    mph = speedMetersPerSecond * 2.23694
    return mph

def DegreesToCardinalDirection(directionDegrees):
    cardinalDirection = "N"
    cardinalDirections = ['N', 'NNE', 'NE', 'ENE',
                          'E', 'ESE', 'SE', 'SSE', 
                          'S', 'SSW', 'SW', 'WSW',
                          'W', 'WNW', 'NW', 'NNW']
    deltaDirection = 11.25
    if directionDegrees >= 0:
        testDirection = 0
        for direction in cardinalDirections:
            testForLowerBound = (testDirection if testDirection > 0 else 360)
            if directionDegrees >= testForLowerBound - deltaDirection and \
               directionDegrees < testDirection + deltaDirection:
                cardinalDirection = direction
            else:
                testDirection += deltaDirection * 2.0
    else:
        # I should probably throw an error
        cardinalDirection = "error"
        
    return cardinalDirection


                          
def LoadWebsiteJson(requestFromThisSite):
    f = requests.get(requestFromThisSite)
    if f.status_code == requests.codes.ok:
        return f
    else:
        print("raising runtime error")
        raise RequestNotOkError("Stats code not okay from request to " + requestFromThisSite)


def ParseCityStateAndForecastSites(requestData):
    cityStateWebsite = 0
    parsed_json = requestData.json()
    parsed_json = requestData.json()
    city = parsed_json['properties']['relativeLocation']['properties']['city']
    state = parsed_json['properties']['relativeLocation']['properties']['state']
    forecastSimpleSite = parsed_json['properties']['forecast']
    forecastHourlySite = parsed_json['properties']['forecastHourly']
    observationsStationsSite = parsed_json['properties']['observationStations']
    observationsSitesRequest = LoadWebsiteJson(observationsStationsSite)
    parsed_sites_json = observationsSitesRequest.json()
    # this is weird, I had to find it in the documentation, because
    # the observationStations didn't contain the actual link to the observations, nor does the
    # features parent contain the link to the obvserations
    closestStationSite = parsed_sites_json['features'][0]['id'] + "/observations"
    closestStationSiteId = parsed_sites_json['features'][0]['properties']['stationIdentifier']
    
    cityStateWebsite = CityStateForecastSites(city, state, forecastSimpleSite,
                                              forecastHourlySite, closestStationSite, closestStationSiteId)
    return cityStateWebsite


def GetCurrentWeather(requestData, city, state, stationID):
    parsed_json = requestData.json()
    # if the station goes down, the return for temperature is null
    dataPoint = 0
    gotTemp = False
    windMetric = 0.0
    windGustMetric = 0.0
    windDirectionDegrees = 0 
    temperature = 0.0
    while (not gotTemp):
        try:
            temp = parsed_json['features'][dataPoint]['properties']['temperature']['value']
            windMetric = parsed_json['features'][dataPoint]['properties']['windSpeed']['value']
            if windMetric is None:
                windMetric = -1
            windGustMetric = parsed_json['features'][dataPoint]['properties']['windGust']['value']
            if windGustMetric is None: 
                windGustMetric = -1 
            windDirectionDegrees = parsed_json['features'][dataPoint]['properties']['windSpeed']['value']
            if windDirectionDegrees is None:
                windDirectionDegrees = -1
            gotTemp = True
        
        except Exception as exc:
            dataPoint += 1
    condition_now = CurrentWeather(
        city + ", " + state,
        0,
        stationID,
        CelsiusToFahrenheit(temperature),
        MetersPerSecondToMilesPerHour(windMetric),
        MetersPerSecondToMilesPerHour(windGustMetric),
        DegreesToCardinalDirection(windDirectionDegrees),
        0,
        0.0
    )
    return condition_now


# requestData is the json string returned from weather.gov
def GetForecastData(requestData, requestedDayName):
    condition_forecast = 0
    parsed_json = requestData.json()

    high = 0.0
    low = 0.0
    windDescr = ""
    simpleForecastDay = ""
    simpleForecastNight = ""
    gothigh = False
    gotlow = False
    for i in range(0, 14):
        dayName = parsed_json['properties']['periods'][i]['name']
        if (requestedDayName.upper() in dayName.upper()):
            if ("night".upper() in dayName.upper() and not "tonight".upper() in dayName.upper()):
                low = parsed_json['properties']['periods'][i]['temperature']
                simpleForecastNight = parsed_json['properties']['periods'][i]['detailedForecast']
                gotlow = True
            else:
                high = parsed_json['properties']['periods'][i]['temperature']
                windDescr = parsed_json['properties']['periods'][i]['windSpeed']
                simpleForecastDay = parsed_json['properties']['periods'][i]['detailedForecast']
                gothigh = True
        if (gothigh and gotlow):
            break
    condition_forecast = FutureWeather(
        high,
        low,
        0.0,
        windDescr,
        requestedDayName,
        simpleForecastDay,
        simpleForecastNight
    )
    return condition_forecast
