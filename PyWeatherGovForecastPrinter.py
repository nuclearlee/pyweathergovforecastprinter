### Print weather data to a terminal
### This is what I started to eventually turn into
### a home weather server. 

import time
import sys
sys.path.append("/home/pi/weatherman/pypiminitftrepo")
import os
import subprocess
import GetWeather as weatherGetter
from GetWeather import RequestNotOkError
import TimeTools as timeTools
from MiniPiTFTDrawing import ImageData
from SetupMiniPiTFT import EnableBacklight, DisableBacklight, MiniTftButtons


def main():
    imageData = ImageData()
    imageData.RotateScreen90Degrees()
    imageData.ClearScreen()
    EnableBacklight()
#    tftButtons = MiniTftButtons()

#    while True:
#        if not tftButtons.buttonA.value and not tftButtons.buttonB.value:
#            print("button A and button B pressed exiting and starting weatherman")
#            print(str(tftButtons.buttonA.value) + str(tftButtons.buttonB.value))
#            break
#        if tftButtons.buttonB.value and not tftButtons.buttonA.value:  # just button A pressed
#            print("only button B pressed")
#        if tftButtons.buttonA.value and not tftButtons.buttonB.value:  # just button B pressed
#            print("only button A pressed")

#    imageData.ClearScreen()
#    imageData.DrawNewLine("buttons" + str(tftButtons.buttonA.value) + str(tftButtons.buttonB.value), (255, 0, 0))
#    imageData.DrawNewLine("hello, I am your", (255, 0, 0))
#    imageData.DrawNewLine("weatherman", (255, 0, 0))
#    imageData.disp.image(imageData.image, imageData.rotation)
#    time.sleep(1)

    sleepDefault = 1800
    sleepToOvercomeServerError = 600
    firstTime = True

    while True:
        timeFromGmtime = time.localtime()
        today = time.strftime("%A", timeFromGmtime)
        hour24Hr = time.strftime("%H", timeFromGmtime)
        minute = time.strftime("%M", timeFromGmtime)
        tomorrow = timeTools.GetTomorrowDayName(today)
        sleepTime = sleepDefault
        try:
            entrySite = weatherGetter.weatherGovEntrySite + "/" + weatherGetter.LOCATION
            loadWeatherGovInfo = weatherGetter.LoadWebsiteJson(entrySite)
            # CityStateForecastSites named tuple
            cityStateSite = weatherGetter.ParseCityStateAndForecastSites(loadWeatherGovInfo)
            if firstTime:
                imageData.DrawNewLine("retrieving data...", (255, 0, 0))
                imageData.disp.image(imageData.image, imageData.rotation)
                firstTime = False
            city, state, simpleSite, hourlySite, closestSite, stationid = cityStateSite
            print("forecast website for " + city + "," + state + ": ")
            print("(simple) " + simpleSite)
            print("(hourly) " + hourlySite)
            print("(closest conditions) " + closestSite)
            loadSimpleForecast = weatherGetter.LoadWebsiteJson(simpleSite)
            loadHourlyForecast = weatherGetter.LoadWebsiteJson(hourlySite)
            loadConditions = weatherGetter.LoadWebsiteJson(closestSite)
            try:
                conditions = weatherGetter.GetCurrentWeather(loadConditions, city, state, stationid)
                forecastTonight = weatherGetter.GetForecastData(loadSimpleForecast, "Tonight")
                forecast = weatherGetter.GetForecastData(loadSimpleForecast, tomorrow)
                imageData.ClearScreen()
                tempNow = "{0:d}\xb0F".format(int(conditions.tempF))
                imageData.DrawNewLine("{0}:{1}".format(hour24Hr, minute) + "  " + tempNow, (255, 255, 255))
                windNow = "wind: "
                if (conditions.windMPH > 0.1):
                    # assume windDir comes unless there is no wind
                    windNow = "{0:d} mph from {1}".format(int(conditions.windMPH), conditions.windDir)
                elif conditions.windMPH >= 0 and conditions.windMPH < 0.1:
                    windNow += "calm"
                else:
                    windNow += "error"

                imageData.DrawNewLine(windNow, (255, 0, 0))
                # the temperature for tonight is parsed as the high
                tempTonight = "Tonight: {0:d}\xb0F".format(int(forecastTonight.tempHigh))
                print(tempTonight)
                imageData.DrawNewLine(tempTonight, (255, 0, 0))
                # draw forecast
                imageData.DrawNewLine("Tomorrow", (12, 195, 97))
                print("temp high" + str(forecast.tempHigh) + "templow" + str(forecast.tempLow))
                imageData.DrawNewLine("{0:d}\xb0F (H), {1:d}\xb0F (L)".format(forecast.tempHigh,forecast.tempLow), (0, 255, 0))
                imageData.DrawNewLine(forecast.windDescr, (0, 255, 0))
                imageData.disp.image(imageData.image, imageData.rotation)
            except Exception as exc:
                print("failed parsing initial set of data: " + exc)
                sleepTime = sleepToOvercomeServerError 
        except RequestNotOkError as exc:
            print(exc)
            sleepTime = sleepToOvercomeServerError
        time.sleep(sleepTime)


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        DisableBacklight()
        try:
            print("keyboard interrupt, exiting")
            sys.exit(1)
        except:
            os._exit(1)
