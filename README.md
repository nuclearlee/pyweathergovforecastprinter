PyWeatherGovForecastPrinter

This is the kick off for my raspberry pi zero weather forecaster. I will use the GetWeather module as the API backend that will interface to some Rasperry GPIO code that controls a 2 line LCD. My current plan is using either a button or some sort of knob(s) that will allow changing the forecast location and from scrolling different forecast days.
