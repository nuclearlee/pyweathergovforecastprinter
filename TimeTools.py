DaysOfTheWeek = [ 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday' ]
ShortDaysOfTheWeek = [ 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun' ]

def GetTomorrowDayName(todayName):
    nextDayIsTomorrow = False;
    for day in DaysOfTheWeek:
        if nextDayIsTomorrow:
            return day
        nextDayIsTomorrow = (todayName.upper() in day.upper())

    return 'Monday'
